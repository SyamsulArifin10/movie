package com.example.Movie.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Movie.exceptions.ResourceNotFoundException;
import com.example.Movie.models.Genre;
import com.example.Movie.modelsDTO.GenreDTO;
import com.example.Movie.repositories.GenreRepository;

@RestController
@RequestMapping("api/")
public class GenreController {
	
	@Autowired
	GenreRepository genreRepository;
	
	//Read All Genre Using DTO
	@GetMapping("genre/readDTO")
	public HashMap<String, Object> readGenreDTO(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ArrayList<Genre> listGenreEntity = (ArrayList<Genre>) genreRepository.findAll();
		
		ArrayList<GenreDTO> listGenreDTO = new ArrayList<GenreDTO>();
		
		for(Genre genre : listGenreEntity) {
			GenreDTO genreDTO = new GenreDTO();
			genreDTO.setId(genre.getId());
			genreDTO.setTitle(genre.getTitle());
			
			listGenreDTO.add(genreDTO);
		}
		result.put("Status", 200);
		result.put("Message", "Read All Genre Data Success");
		result.put("Data", listGenreDTO);
		
		return result;
	}
	//Read All Genre
	@GetMapping("genre/readModelMapper")
	public HashMap<String, Object> readGenreModelMapper(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ArrayList<Genre> listGenreEntity = (ArrayList<Genre>) genreRepository.findAll();
		
		ArrayList<GenreDTO> listGenreDTO = new ArrayList<GenreDTO>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		for(Genre genre : listGenreEntity) {
			GenreDTO genreDTO = modelMapper.map(genre, GenreDTO.class);
			
			listGenreDTO.add(genreDTO);
		}
		
		result.put("Status", 200);
		result.put("Message", "Read All Genre Data Success");
		result.put("Data", listGenreDTO);
		
		return result;
	}
	
	//Create Genre Using DTO
	@PostMapping("genre/createDTO")
	public HashMap<String, Object> createGenreDTO(@Valid @RequestBody GenreDTO genreDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Genre genre = new Genre();
		genre.setId(genreDTO.getId());
		genre.setTitle(genreDTO.getTitle());
		
		genreRepository.save(genre);
		
		genreDTO.setId(genre.getId());
		
		result.put("Status", 200);
		result.put("Message", "Create Genre Success");
		result.put("Data", genreDTO);
		
		return result;
	}
	
	//Create a Genre
	@PostMapping("genre/createModelMapper")
	public HashMap<String, Object> createGenreModelMapper(@Valid @RequestBody GenreDTO genreDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		Genre genre = modelMapper.map(genreDTO, Genre.class);
		
		genreRepository.save(genre);
		
		genreDTO.setId(genre.getId());
		
		result.put("Status", 200);
		result.put("Message", "Create New Genre Success");
		result.put("Data", genreDTO);
		
		return result;
	}
	
	//Update A Genre Using DTO
	@PutMapping("genre/updateDTO/{id}")
	public HashMap<String, Object> updateGenreDTO(@PathVariable(value = "id")long genreID, @Valid @RequestBody GenreDTO genreDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Genre genre = genreRepository.findById(genreID)
				.orElseThrow(() -> new ResourceNotFoundException("Genre", "id", genreID));
		genre.setTitle(genreDTO.getTitle());
		
		genreRepository.save(genre);
		
		genreDTO.setId(genre.getId());
		
		result.put("Status", 200);
		result.put("Message", "Update Genre Success");
		result.put("Data", genreDTO);
		
		return result;
	}
	
	//Update A Genre
	@PutMapping("genre/updateModelMapper/{id}")
	public HashMap<String, Object> updateGenreModelMapper (@PathVariable (value = "id")long genreID,@Valid @RequestBody GenreDTO genreDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		Genre genre = genreRepository.findById(genreID)
				.orElseThrow(() -> new ResourceNotFoundException("Genre", "id", genreID));
		genre = modelMapper.map(genreDTO, Genre.class);
		genre.setId(genreID);
		genreRepository.save(genre);
		
		genreDTO = modelMapper.map(genre, GenreDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update Genre Success");
		result.put("Data", genreDTO);
		
		return result;
	}
	
	//Delete Genre Using DTO
	@DeleteMapping("genre/deleteDTO/{id}")
	public HashMap<String, Object> deleteGenreDTO(@PathVariable(value = "id")long genreID){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Genre genre = genreRepository.findById(genreID)
				.orElseThrow(() -> new ResourceNotFoundException("Genre", "id", genreID));
		
		genreRepository.delete(genre);
		
		GenreDTO genreDTO = new GenreDTO();
		genreDTO.setId(genre.getId());
		genreDTO.setTitle(genre.getTitle());
		
		result.put("Status", 200);
		result.put("Message", "Delete Genre Success");
		result.put("Data", genreDTO);
		
		return result;
	}
	//Delete A Genre
	@DeleteMapping("genre/deleteModelMapper/{id}")
	public HashMap<String, Object> deleteGenreModelMapper(@PathVariable(value = "id")long genreID){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		Genre genre = genreRepository.findById(genreID)
				.orElseThrow(() -> new ResourceNotFoundException("Genre", "id", genreID));
		
		genreRepository.delete(genre);
		
		GenreDTO genreDTO = modelMapper.map(genre, GenreDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Delete Genre Success");
		result.put("Data", genreDTO);
		
		return result;
	}

}
