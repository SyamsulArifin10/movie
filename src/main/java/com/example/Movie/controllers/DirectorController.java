package com.example.Movie.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Movie.exceptions.ResourceNotFoundException;
import com.example.Movie.models.Director;
import com.example.Movie.modelsDTO.DirectorDTO;
import com.example.Movie.repositories.DirectorRepository;

@RestController
@RequestMapping("/api")
public class DirectorController {
	
	@Autowired
	DirectorRepository directorRepository;
	
	//Read All Director
	@GetMapping("director/readModelMapper")
	public HashMap<String, Object> readAllDirector(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		ArrayList<Director> listDirectorEntity = (ArrayList<Director>) directorRepository.findAll();
		
		ArrayList<DirectorDTO> listDirectorDTO = new ArrayList<DirectorDTO>();
		
		for(Director director : listDirectorEntity) {
			DirectorDTO directorDTO = modelMapper.map(director, DirectorDTO.class);
			
			listDirectorDTO.add(directorDTO);
		}
		
		result.put("Status", 200);
		result.put("Message", "Read All Director Data Success");
		result.put("Data", listDirectorDTO);
		
		return result;
	}
	
	//Read All Director Using DTO
	@GetMapping("director/readDTO")
	public HashMap<String, Object> getAllDirectorDTO(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ArrayList<Director> listDirectorEntity = (ArrayList<Director>) directorRepository.findAll();
		
		ArrayList<DirectorDTO> listDirectorDTO = new ArrayList<DirectorDTO>();
		
		for(Director director : listDirectorEntity) {
			DirectorDTO directorDTO = new DirectorDTO();
			
			directorDTO.setFirstName(director.getFirstName());
			directorDTO.setLastName(director.getLastName());
			directorDTO.setId(director.getId());
			
			listDirectorDTO.add(directorDTO);
		}
		
		result.put("Status", 200);
		result.put("Message", "Read All Director Data Success");
		result.put("Data", listDirectorDTO);
		
		return result;
	}
	
	//Create a Director using DTO
	@PostMapping("director/createDTO")
	public HashMap<String, Object> createDirectorDTO(@Valid @RequestBody DirectorDTO directorDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Director director = new Director();
		director.setFirstName(directorDTO.getFirstName());
		director.setLastName(directorDTO.getLastName());
		
		directorRepository.save(director);
		
		directorDTO.setId(director.getId());
		
		result.put("Status", 200);
		result.put("Message", "Create New Director Success");
		result.put("Data", directorDTO);
		
		return result;
	}
	
	//Create a Director
	@PostMapping("director/createModelMapper")
	public HashMap<String, Object> createDirectorModelMapper(@Valid @RequestBody DirectorDTO directorDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		Director director = modelMapper.map(directorDTO, Director.class);
		
		directorRepository.save(director);
		
		directorDTO.setId(director.getId());
		
		result.put("Status", 200);
		result.put("Message", "Create New Director Success");
		result.put("Data", directorDTO);
		
		return result;
	}
	
	//Update a Director using DTO
	@PutMapping("director/updateDTO/{id}")
	public HashMap<String, Object> updateDirectorDTO(@PathVariable(value = "id")long directorID, @Valid @RequestBody DirectorDTO directorDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Director director = directorRepository.findById(directorID)
				.orElseThrow(() -> new ResourceNotFoundException("Director", "id", directorID));
		
		director.setFirstName(directorDTO.getFirstName());
		director.setLastName(directorDTO.getLastName());
		director.setId(directorID);
		
		directorRepository.save(director);
		
		directorDTO.setFirstName(director.getFirstName());
		directorDTO.setLastName(director.getLastName());
		directorDTO.setId(director.getId());
		
		result.put("Status", 200);
		result.put("Message", "Update Director Success");
		result.put("Data", directorDTO);
		
		return result;
	}
	
	//Update a Director
	@PutMapping("director/updateModelMapper/{id}")
	public HashMap<String, Object> updateModelMapper(@PathVariable(value = "id")long directorID, @Valid @RequestBody DirectorDTO directorDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		Director director = directorRepository.findById(directorID)
				.orElseThrow(() -> new ResourceNotFoundException("Director", "id", directorID));
		director = modelMapper.map(directorDTO, Director.class);
		director.setId(directorID);
		
		directorRepository.save(director);
		
		directorDTO = modelMapper.map(director, DirectorDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update Director Success");
		result.put("Data", directorDTO);
		
		return result;
	}
	
	//Delete a Director Using DTO
	@DeleteMapping("director/deleteDTO/{id}")
	public HashMap<String, Object> deleteDirectorDTO(@PathVariable(value = "id")long directorID){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Director director = directorRepository.findById(directorID)
				.orElseThrow(() -> new ResourceNotFoundException("Director", "id", directorID));
		
		directorRepository.delete(director);
		
		DirectorDTO directorDTO = new DirectorDTO();
		
		directorDTO.setFirstName(director.getFirstName());
		directorDTO.setId(director.getId());
		directorDTO.setLastName(director.getLastName());
		
		result.put("Status", 200);
		result.put("Message", "Delete Director Success");
		result.put("Data", directorDTO);
		
		return result;
	}
	
	//Delete a Director
	@DeleteMapping("director/deleteModelMapper/{id}")
	public HashMap<String, Object> deleteModelMapper(@PathVariable(value = "id")long directorID){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		Director director = directorRepository.findById(directorID)
				.orElseThrow(() -> new ResourceNotFoundException("Director", "id", directorID));
		
		directorRepository.delete(director);
		
		DirectorDTO directorDTO = modelMapper.map(director, DirectorDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Delete Director Success");
		result.put("Data", directorDTO);
		
		return result;
	}

}
