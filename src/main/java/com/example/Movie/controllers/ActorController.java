package com.example.Movie.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Movie.models.Actor;
import com.example.Movie.modelsDTO.ActorDTO;
import com.example.Movie.exceptions.ResourceNotFoundException;
import com.example.Movie.repositories.ActorRepository;

@RestController
@RequestMapping("/api")
public class ActorController {

	@Autowired
	ActorRepository actorRepository;
	
	//Read All Actor
	@GetMapping("actor/readModelMapper")
	public HashMap<String, Object> getAllActorModelMapper(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		ArrayList<Actor> listActorEntity = (ArrayList<Actor>) actorRepository.findAll();
		
		ArrayList<ActorDTO> listActorDTO = new ArrayList<ActorDTO>();
		
		for(Actor actor : listActorEntity) {
			ActorDTO actorDTO = new ActorDTO();
			
			actorDTO = modelMapper.map(actor, ActorDTO.class);
			
			listActorDTO.add(actorDTO);
		}
		
		result.put("Status", 200);
		result.put("Message", "Read All Actor Data Success");
		result.put("Data", listActorDTO);
		
		return result;
	}
	
	//Read All Actor
	@GetMapping("actor/readDTO")
	public HashMap<String, Object> createActorDTO(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ArrayList<Actor> listActorEntity =(ArrayList<Actor>) actorRepository.findAll();
		
		ArrayList<ActorDTO> listActorDTO = new ArrayList<ActorDTO>();
		
		for(Actor actor : listActorEntity) {
			ActorDTO actorDTO = new ActorDTO();
			
			actorDTO.setFirstName(actor.getFirstName());
			actorDTO.setGender(actor.getGender());
			actorDTO.setLastName(actor.getLastName());
			actorDTO.setId(actor.getId());
			
			listActorDTO.add(actorDTO);
		}
		
		result.put("Status", 200);
		result.put("Message", "Read All Actor Data Success");
		result.put("Data", listActorDTO);
		
		return result;
	}
	
	//Create a Actor
	@PostMapping("actor/createModelMapper")
	public HashMap<String, Object> createActorModelMapper(@Valid @RequestBody ActorDTO actorDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		Actor actorEntity = modelMapper.map(actorDTO, Actor.class);
		
		actorRepository.save(actorEntity);
		
		actorDTO.setId(actorEntity.getId());
		
		result.put("Status", 200);
		result.put("Message", "Create New Actor Success");
		result.put("Data", actorDTO);
		
		return result;
	}
	
	//Create a Actor
	@PostMapping("actor/createDTO")
	public HashMap<String, Object> createActorDTO(@Valid @RequestBody ActorDTO actorDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Actor actor = new Actor();
		
		actor.setFirstName(actorDTO.getFirstName());
		actor.setGender(actorDTO.getGender());
		actor.setLastName(actorDTO.getLastName());
		
		actorRepository.save(actor);
		
		actorDTO.setId(actor.getId());
		
		result.put("Status", 200);
		result.put("Message", "Create New Actor Success");
		result.put("Data", actorDTO);
		
		return result;
	}
	
	//Update a Actor
	@PutMapping("actor/updateModelMapper/{id}")
	public HashMap<String, Object> updateActorModelMapper(@PathVariable(value = "id")long actorID, @Valid @RequestBody ActorDTO actorDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		Actor actorEntity = actorRepository.findById(actorID)
				.orElseThrow(() -> new ResourceNotFoundException("Actor", "id", actorID));
		
		actorEntity = modelMapper.map(actorDTO, Actor.class);
		actorEntity.setId(actorID);
		
		actorRepository.save(actorEntity);
		
		actorDTO = modelMapper.map(actorEntity, ActorDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update Actor Success");
		result.put("Data", actorDTO);
		
		return result;
	}
	
	//Update a Actor
	@PutMapping("actor/updateDTO/{id}")
	public HashMap<String, Object> updateActorDTO(@PathVariable(value = "id")long actorID, @Valid @RequestBody ActorDTO actorDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Actor actor = actorRepository.findById(actorID)
				.orElseThrow(() -> new ResourceNotFoundException("Actor", "id", actorID));
		
		actor.setFirstName(actorDTO.getFirstName());
		actor.setLastName(actorDTO.getLastName());
		actor.setGender(actorDTO.getGender());
		
		actorRepository.save(actor);
		
		actorDTO.setId(actor.getId());
		
		result.put("Status", 200);
		result.put("Message", "Update Actor Success");
		result.put("Data", actorDTO);
		
		return result;
	}
	
	//Delete a Actor
	@DeleteMapping("actor/deleteModelMapper/{id}")
	public HashMap <String, Object> deleteActorModelMapper(@PathVariable(value = "id")long actorID){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		Actor actorEntity = actorRepository.findById(actorID)
				.orElseThrow(() -> new ResourceNotFoundException("Actor", "id", actorID));
		
		actorRepository.delete(actorEntity);
		
		ActorDTO actorDTO = modelMapper.map(actorEntity, ActorDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Delete Actor Success");
		result.put("Data", actorDTO);
		
		return result;
	}
	
	//Delete a Actor
	@DeleteMapping("actor/deleteDTO/{id}")
	public HashMap<String, Object> deleteActorDTO(@PathVariable(value = "id")long actorID){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Actor actor = actorRepository.findById(actorID)
				.orElseThrow(() -> new ResourceNotFoundException("Actor", "id", actorID));
		
		actorRepository.delete(actor);
		
		ActorDTO actorDTO = new ActorDTO();
		actorDTO.setFirstName(actor.getFirstName());
		actorDTO.setGender(actor.getGender());
		actorDTO.setLastName(actor.getLastName());
		actorDTO.setId(actor.getId());
		
		result.put("Status", 200);
		result.put("Message", "Delete Author Success");
		result.put("Data", actorDTO);
		
		return result;
	}
}
