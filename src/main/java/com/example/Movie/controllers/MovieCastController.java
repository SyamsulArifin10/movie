package com.example.Movie.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Movie.models.MovieCast;
import com.example.Movie.modelsDTO.MovieCastDTO;
import com.example.Movie.repositories.MovieCastRepository;

@RestController
@RequestMapping("api/")
public class MovieCastController {
	
	@Autowired
	MovieCastRepository movieCastRepository;
	
	//Read All Movie Cast
	@GetMapping("moviecast/readModelMapper")
	public HashMap<String, Object> readMovieCastModelMapper(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		ArrayList<MovieCast> listMovieCastEntity = (ArrayList<MovieCast>) movieCastRepository.findAll();
		
		ArrayList<MovieCastDTO> listMovieCastDTO = new ArrayList<MovieCastDTO>();
		
		for(MovieCast movieCast : listMovieCastEntity) {
			MovieCastDTO movieCastDTO = modelMapper.map(movieCast, MovieCastDTO.class);
			
			listMovieCastDTO.add(movieCastDTO);
		}
		
		result.put("Status", 200);
		result.put("Message", "Read All Movie Cast Data Success");
		result.put("Data", listMovieCastDTO);
		
		return result;
	}
	
	//Create A Movie Cast
	@PostMapping("moviecast/createModelMapper")
	public HashMap<String, Object> createMovieCastModelMapper(@Valid @RequestBody MovieCastDTO movieCastDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		MovieCast movieCast = modelMapper.map(movieCastDTO, MovieCast.class);
		
		movieCastRepository.save(movieCast);
		
		movieCastDTO.setId(movieCast.getId());
		
		result.put("Status", 200);
		result.put("Message", "Create New Movie Cast Success");
		result.put("Data", movieCastDTO);
		
		return result;
	}
	
	//Update A Movie Cast
	@PutMapping("moviecast/updateModelMapper/{actID}/{movID}")
	public HashMap<String, Object> updateMovieCastModelMapper(@PathVariable(value = "actID")long actID,@PathVariable(value = "movID") long movID, @Valid @RequestBody MovieCastDTO movieCastDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		boolean isFound = false;
		
		ArrayList<MovieCast> listMovieCastEntity = (ArrayList<MovieCast>) movieCastRepository.findAll();
		
		ModelMapper modelMapper = new ModelMapper();
		
		for(MovieCast movieCast : listMovieCastEntity) {
			if(movieCast.getId().getActId() == actID && movieCast.getId().getMovId() == movID) {
				movieCast.setRole(movieCastDTO.getRole());
				
				movieCastRepository.save(movieCast);
				
				movieCastDTO = modelMapper.map(movieCast, MovieCastDTO.class);
				isFound = true;
			}
			else {
				isFound = false;
			}
		}
		if(isFound == true) {
			result.put("Status", 200);
			result.put("Message", "Update Movie Cast Success");
			result.put("Data", movieCastDTO);
		}
		else if(isFound == false) {
			result.put("Error", 400);
			result.put("Message", "Update Movie Cast NOT Success");
			result.put("Data", listMovieCastEntity);
		}
		
		return result;
	}
	
	//Delete a Movie Cast
	@DeleteMapping("moviecast/deleteModelMapper/{actID}/{movID}")
	public HashMap<String, Object> deleteMovieCastModelMapper(@PathVariable(value = "actID")long actID,@PathVariable(value = "movID")long movID ){
		HashMap<String, Object> result = new HashMap<String, Object>();
		boolean isFound = false;
		
		ModelMapper modelMapper = new ModelMapper();
		
		MovieCastDTO movieCastDTO = new MovieCastDTO();
		
		ArrayList<MovieCast> listMovieCastEntity = (ArrayList<MovieCast>) movieCastRepository.findAll();
		for(MovieCast movieCast : listMovieCastEntity) {
			if(movieCast.getId().getActId() == actID && movieCast.getId().getMovId() == movID) {
				movieCastRepository.delete(movieCast);
				
				movieCastDTO = modelMapper.map(movieCast, MovieCastDTO.class);
				isFound = true;
			}
			else {
				isFound = false;
			}
		}
		if(isFound) {
			result.put("Status", 200);
			result.put("Message", "Delete Movie Cast Success");
			result.put("Data", movieCastDTO);
		}
		else if(!isFound) {
			result.put("Error", 400);
			result.put("Message", "Delete Movie Cast NOT Success");
			result.put("Data", listMovieCastEntity);
		}
		return result;
	}

}
