package com.example.Movie.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Movie.exceptions.ResourceNotFoundException;
import com.example.Movie.models.Movie;
import com.example.Movie.modelsDTO.MovieDTO;
import com.example.Movie.repositories.MovieRepository;

@RestController
@RequestMapping("api/")
public class MovieController {
	
	@Autowired
	MovieRepository movieRepository;
	
	//Read All Movie
	@GetMapping("movie/readModelMapper")
	public HashMap<String, Object> readMovieModelMapper(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		ArrayList<Movie> listMovieEntity = (ArrayList<Movie>) movieRepository.findAll();
		
		ArrayList<MovieDTO> listMovieDTO = new ArrayList<MovieDTO>();
		
		for(Movie movie : listMovieEntity) {
			MovieDTO movieDTO = modelMapper.map(movie, MovieDTO.class);
			
			listMovieDTO.add(movieDTO);
		}
		
		result.put("Status", 200);
		result.put("Message", "Read All Data Movie Success");
		result.put("Data", listMovieDTO);
		
		return result;
	}
	//Read All Movie Using DTO
	@GetMapping("movie/readDTO")
	public HashMap<String, Object> readMovieDTO(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ArrayList<Movie> listMovieEntity = (ArrayList<Movie>) movieRepository.findAll();
		
		ArrayList<MovieDTO> listMovieDTO = new ArrayList<MovieDTO>();
		
		for(Movie movie : listMovieEntity) {
			MovieDTO movieDTO = new MovieDTO();
			
			movieDTO.setDateRelease(movie.getDateRelease());
			movieDTO.setId(movie.getId());
			movieDTO.setLanguage(movie.getLanguage());
			movieDTO.setReleaseCountry(movie.getReleaseCountry());
			movieDTO.setTime(movie.getTime());
			movieDTO.setTitle(movie.getTitle());
			movieDTO.setYear(movie.getYear());
			
			listMovieDTO.add(movieDTO);
		}
		
		result.put("Status", 200);
		result.put("Message", "Read All Data Movie Success");
		result.put("Data", listMovieDTO);
		
		return result;
	}
	
	//Create a Movie
	@PostMapping("movie/createModelMapper")
	public HashMap<String, Object> createMovieModelMapper(@Valid @RequestBody MovieDTO movieDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		Movie movie = modelMapper.map(movieDTO, Movie.class);
		
		movieRepository.save(movie);
		
		movieDTO.setId(movie.getId());
		
		result.put("Status", 200);
		result.put("Message", "Create New Movie Success");
		result.put("Data", movieDTO);
		
		return result;
	}
	
	//Create new Movie using DTO
	@PostMapping("movie/createDTO")
	public HashMap<String, Object> createMovieDTO(@Valid @RequestBody MovieDTO movieDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Movie movie = new Movie();
		movie.setDateRelease(movieDTO.getDateRelease());
		movie.setLanguage(movieDTO.getLanguage());
		movie.setReleaseCountry(movieDTO.getReleaseCountry());
		movie.setTime(movieDTO.getTime());
		movie.setTitle(movieDTO.getTitle());
		movie.setYear(movieDTO.getYear());
		
		movieRepository.save(movie);
		
		movieDTO.setId(movie.getId());
		
		result.put("Status", 200);
		result.put("Message", "Create New Movie Success");
		result.put("Data", movieDTO);
		
		return result;
	}
	
	//Update a movie
	@PutMapping("movie/updateModelMapper/{id}")
	public HashMap<String, Object> updateMovieModelMapper(@PathVariable(value = "id")long movieID, @Valid @RequestBody MovieDTO movieDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		Movie movie = movieRepository.findById(movieID)
				.orElseThrow(() -> new ResourceNotFoundException("Movie", "id", movieID));
		
		movie = modelMapper.map(movieDTO, Movie.class);
		movie.setId(movieID);
		movieRepository.save(movie);
		
		movieDTO = modelMapper.map(movie, MovieDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update Movie Success");
		result.put("Data", movieDTO);
		
		return result;
	}
	
	//Update A movie using DTO
	@PutMapping("movie/updateDTO/{id}")
	public HashMap<String, Object> updateMovieDTO(@PathVariable(value = "id")long movieID,@Valid @RequestBody MovieDTO movieDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Movie movie = movieRepository.findById(movieID)
				.orElseThrow(() -> new ResourceNotFoundException("Movie", "id", movieID));
		movie.setDateRelease(movieDTO.getDateRelease());
		movie.setId(movieID);
		movie.setLanguage(movieDTO.getLanguage());
		movie.setReleaseCountry(movieDTO.getReleaseCountry());
		movie.setTime(movieDTO.getTime());
		movie.setTitle(movieDTO.getTitle());
		movie.setYear(movieDTO.getYear());
		
		movieRepository.save(movie);
		
		movieDTO.setId(movie.getId());
		
		result.put("Status", 200);
		result.put("Message", "Update Movie Success");
		result.put("Data", movieDTO);
		
		return result;
	}
	
	//Delete A Movie
	@DeleteMapping("movie/deleteModelMapper/{id}")
	public HashMap<String, Object> deleteMovieModelMapper(@PathVariable(value = "id")long movieID){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Movie movie = movieRepository.findById(movieID)
				.orElseThrow(() -> new ResourceNotFoundException("Movie", "id", movieID));
		
		movieRepository.delete(movie);
		
		MovieDTO movieDTO = modelMapper.map(movie, MovieDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Delete Movie Success");
		result.put("Data", movieDTO);
		
		return result;
	}
	
	//Delete A Movie Using DTO 
	@DeleteMapping("movie/deleteDTO/{id}")
	public HashMap<String, Object> deleteMovieDTO(@PathVariable(value = "id")long movieID){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Movie movie = movieRepository.findById(movieID)
				.orElseThrow(() -> new ResourceNotFoundException("Movie", "id", movieID));
		
		movieRepository.delete(movie);
		
		MovieDTO movieDTO = new MovieDTO();
		movieDTO.setDateRelease(movie.getDateRelease());
		movieDTO.setId(movie.getId());
		movieDTO.setLanguage(movie.getLanguage());
		movieDTO.setReleaseCountry(movie.getReleaseCountry());
		movieDTO.setTime(movie.getTime());
		movieDTO.setTitle(movie.getTitle());
		movieDTO.setYear(movie.getYear());
		
		result.put("Status", 200);
		result.put("Message", "Delete Movie Success");
		result.put("Data", movieDTO);
		
		return result;
	}
}
