package com.example.Movie.modelsDTO;

public class DirectorDTO {

	private long id;
	private String firstName;
	private String lastName;

	public DirectorDTO() {
	}

	public DirectorDTO(long id) {
		this.id = id;
	}

	public DirectorDTO(long id, String firstName, String lastName) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
}