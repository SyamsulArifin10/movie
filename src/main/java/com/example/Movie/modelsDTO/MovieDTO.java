package com.example.Movie.modelsDTO;

import java.util.Date;

public class MovieDTO {

	private long id;
	private Date dateRelease;
	private String language;
	private String releaseCountry;
	private int time;
	private String title;
	private int year;

	public MovieDTO() {
	}

	public MovieDTO(long id, int time, int year) {
		this.id = id;
		this.time = time;
		this.year = year;
	}

	public void Movie(long id, Date dateRelease, String language, String releaseCountry, int time, String title, int year) {
		this.id = id;
		this.dateRelease = dateRelease;
		this.language = language;
		this.releaseCountry = releaseCountry;
		this.time = time;
		this.title = title;
		this.year = year;
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getDateRelease() {
		return this.dateRelease;
	}

	public void setDateRelease(Date dateRelease) {
		this.dateRelease = dateRelease;
	}

	public String getLanguage() {
		return this.language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getReleaseCountry() {
		return this.releaseCountry;
	}

	public void setReleaseCountry(String releaseCountry) {
		this.releaseCountry = releaseCountry;
	}

	public int getTime() {
		return this.time;
	}

	public void setTime(int time) {
		this.time = time;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getYear() {
		return this.year;
	}

	public void setYear(int year) {
		this.year = year;
	}
}

