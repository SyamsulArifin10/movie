package com.example.Movie.modelsDTO;

public class ActorDTO {

	private long id;
	private String firstName;
	private String gender;
	private String lastName;

	public ActorDTO() {
	}

	public ActorDTO(long id) {
		this.id = id;
	}

	public ActorDTO(long id, String firstName, String gender, String lastName) {
		this.id = id;
		this.firstName = firstName;
		this.gender = gender;
		this.lastName = lastName;
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
}
