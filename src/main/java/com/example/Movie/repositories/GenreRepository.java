package com.example.Movie.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.Movie.models.Genre;

@Repository
public interface GenreRepository extends JpaRepository<Genre, Long> {

}
